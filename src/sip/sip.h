
#include <osmocom/core/timer.h>
#include <osmocom/cc/endpoint.h>
#include <sofia-sip/nua.h>
#include <sofia-sip/stun.h>

#define STUN_RETRY_TIMER	10
#define REGISTER_RETRY_TIMER	10

enum reg_state {
	REGISTER_STATE_NULL = 0,
	REGISTER_STATE_UNREGISTERED,
	REGISTER_STATE_REGISTERING,
	REGISTER_STATE_REGISTERED,
	REGISTER_STATE_FAILED,
};

enum stun_state {
	STUN_STATE_NULL = 0,
	STUN_STATE_UNRESOLVED,
	STUN_STATE_RESOLVING,
	STUN_STATE_RESOLVED,
	STUN_STATE_FAILED,
};

// keep in sync with string array in sip.c!
enum sip_state {
	SIP_STATE_IDLE = 0,
	SIP_STATE_OUT_INVITE,	/* invite sent, waiting for replies */
	SIP_STATE_IN_INVITE,	/* invite received, sending replies */
	SIP_STATE_CONNECT,	/* active call */
	SIP_STATE_OUT_DISC,	/* outgoing disconnect, sending REL_IND */
	SIP_STATE_OUT_RELEASE,	/* outgoing release, sending REL_CNF */
};

struct sip_call;

typedef struct sip_endpoint {
	/* setting flags */
	int		send_no_ringing_after_progress;
	int		receive_no_ringing_after_progress;

	/* endpoint */
	osmo_cc_endpoint_t cc_ep;
	const char	*name;
	struct sip_call	*call_list;

	/* SIP settings */
	const char	*local_user;
	const char	*local_peer;
	const char	*remote_user;
	const char	*registered_user;
	const char	*remote_peer;
	const char	*asserted_id;
	int		local_register;
	int		remote_register;
	int		remote_nat_sip;
	int		remote_nat_rtp;
	const char	*register_user;
	const char	*register_peer;
	int		authenticate_local;
	int		authenticate_remote;
	const char	*auth_user;
	const char	*auth_password;
	const char	*auth_realm;
	const char	*block_failure;
	const char	*remote_user_agent;

	/* NAT help */
	char		public_ip[256];
	const char	*stun_server;

	/* timers */
	int		register_interval;
	int		options_interval;
	int		stun_interval;
	struct osmo_timer_list	stun_retry_timer;
	struct osmo_timer_list	register_retry_timer;
	struct osmo_timer_list	register_expire_timer;
	struct osmo_timer_list	register_option_timer;

	/* SIP stack */
	su_root_t	*su_root;
	nua_t		*nua;
	nua_handle_t	*register_handle;
	int		shutdown_complete;

	/* register process */
	char		remote_contact_user[256];
	char		remote_contact_peer[256];
	char		register_nonce[64];
	char		invite_nonce[64];
	enum reg_state	register_state;
	enum stun_state	stun_state;

	/* stun process */
	stun_handle_t	*stun_handle;
	su_socket_t	stun_socket;
} sip_endpoint_t;

typedef struct sip_call {
	struct sip_call *next;

	osmo_cc_call_t	*cc_call;
	uint32_t	cc_callref;
	sip_endpoint_t	*sip_ep;
	enum sip_state	state;

	nua_handle_t	*nua_handle;

	struct osmo_timer_list	invite_option_timer;

	char		*sdp_request, *sdp_response;
	int		sdp_sent;
	int		alerting_sent;

} call_t;

void cc_message(osmo_cc_endpoint_t *ep, uint32_t callref, osmo_cc_msg_t *msg);
sip_endpoint_t *sip_endpoint_create(const char *user_agent, const char *remote_user_agent, int send_no_ringing_after_progress, int receive_no_ringing_after_progress, const char *name, const char *local_user, const char *local_peer, const char *remote_user, const char *remote_peer, const char *asserted_id, int local_register, int remote_register, int remote_nat_sip, int remote_nat_rtp, const char *register_user, const char *register_peer, int authenticate_local, int authenticate_remote, const char *auth_user, const char *auth_password, const char *auth_realm, const char *public_ip, const char *stun_server, int register_interval, int options_interval, int stun_interval, int expires, const char *block_failure);
void sip_endpoint_destroy(sip_endpoint_t *sip_ep);
int sip_init(int debug_level);
void sip_exit(void);
void sip_handle(sip_endpoint_t *sip_ep);

